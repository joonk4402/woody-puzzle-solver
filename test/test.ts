import { expect } from 'chai';
import * as mocha from 'mocha';
import {
	findPieceIndexes,
	getPossibleMoves,
	pieces,
	placePieceIfValid,
	removeFullRows
} from '../src/helpers/solver';
import * as _ from 'lodash';
import { Board } from '../src/helpers/board';
import { Piece } from '../src/helpers/piece';
import { MoveInfo } from '../src/helpers/entity';

describe('getPossibleMoves', function () {
	it('Should get all possible boards for vertical piece and empty board', function () {
		const moves: MoveInfo[] = getPossibleMoves(new Board(), pieces[2], 2);

		compareBoards(moves[0].board, new Board([
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		]));

		compareBoards(_.last(moves).board, new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
		]));
	});

	it('Should get all possible boards for square piece and filled board', function () {
		const moves: MoveInfo[] = getPossibleMoves(new Board([
			[1, 0, 1, 1, 1, 1, 1, 1, 1, 0],
			[1, 0, 1, 1, 1, 1, 1, 0, 1, 1],
			[1, 0, 1, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 1, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 1, 1, 1, 1, 1, 1, 1, 0],
			[1, 0, 1, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
			[1, 0, 1, 1, 0, 0, 0, 1, 0, 1],
			[1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		]), pieces[7], 1);

		compareBoards(moves[0].board, new Board([
			[1, 0, 0, 1, 1, 1, 1, 1, 1, 0],
			[1, 0, 0, 1, 1, 1, 1, 0, 1, 1],
			[1, 0, 0, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 0, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 0, 1, 1, 1, 1, 1, 1, 0],
			[1, 0, 0, 1, 0, 1, 1, 1, 1, 1],
			[1, 0, 0, 1, 0, 1, 1, 1, 0, 1],
			[1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
		]));
	});


});

describe('placePieceIfValid', function () {
	it('Should place long vertical piece on board at 0,0 location', function () {
		let move: MoveInfo = placePieceIfValid(new Board(), pieces[2], 0, 0);
		compareBoards(move.board, new Board([
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		]));
	});
	it('Should place long vertical piece on board at specific location', function () {
		let move: MoveInfo = placePieceIfValid(new Board(), pieces[2], 5, 9);
		compareBoards(move.board, new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
		]));
	});
});

describe('removeFullRows', function () {
	it('Should remove a full row', function () {
		let board = new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
		]);
		removeFullRows(board);
		compareBoards(board, new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
		]));
	});

	it('Should remove a full column', function () {
		let board = new Board([
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 1, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 1, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 1, 0, 0, 0]
		]);
		removeFullRows(board);
		compareBoards(board, new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
		]));
	});

	it('Should remove both full rows and column', function () {
		let board = new Board([
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 1, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 1, 0],
			[0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
			[0, 0, 1, 1, 0, 0, 1, 0, 0, 0]
		]);
		removeFullRows(board);
		compareBoards(board, new Board([
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
			[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]
		]));
	});
});

describe('test find piece indexes', function () {
	it('Should find index of the three pieces', function () {
		let firstPiece = new Piece([
			[1, 1, 0, 0, 0],
			[1, 1, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
		]);
		let secondPiece = new Piece([
			[1, 1, 0, 0, 0],
			[1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
		]);
		let thirdPiece = new Piece([
			[1, 0, 0, 0, 0],
			[1, 0, 0, 0, 0],
			[1, 0, 0, 0, 0],
			[1, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
		]);
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [firstPiece, secondPiece, thirdPiece]).toString()).to.equal([0, 1, 2].toString());
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [firstPiece, thirdPiece, secondPiece]).toString()).to.equal([0, 2, 1].toString());
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [secondPiece, firstPiece, thirdPiece]).toString()).to.equal([1, 0, 2].toString());
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [secondPiece, thirdPiece, firstPiece]).toString()).to.equal([1, 2, 0].toString());
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [thirdPiece, secondPiece, firstPiece]).toString()).to.equal([2, 1, 0].toString());
		expect(findPieceIndexes([firstPiece, secondPiece, thirdPiece], [thirdPiece, firstPiece, secondPiece]).toString()).to.equal([2, 0, 1].toString());
	});
});

// describe('getBoardScoreAi', function() {
//   it('Should return higher score for board that allows more pieces', function() {
//     let score
//     expect().to.
//     let move: MoveInfo = placePieceIfValid(new Board(), pieces[2], 0, 0);
//     compareBoards(move.board, new Board([
//       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//       [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
//       [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
//       [1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
//       [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
//       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
//       [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
//       [1, 1, 1, 1, 1, 1, 1, 0, 1, 1],
//       [1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
//       [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
//     ]));
//   });
// });

function compareBoards(board: Board, expectedBoard: Board): void {
	expect(board.toString()).to.equal(expectedBoard.toString());
}

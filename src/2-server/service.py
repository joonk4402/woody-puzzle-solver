import cherrypy
import pandas as pd
import json
from pandas.io.json import json_normalize
from ludwigproxy import LudwigProxy
from recursiveerror import RecursiveError

# https://towardsdatascience.com/build-your-own-python-restful-web-service-840ed7766832

p = LudwigProxy()


class AiWebService(object):

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def train(self):
        data = cherrypy.request.json
        if 'training_samples' not in data:
            return RecursiveError('"training_samples" property is required on body to begin training').to_json()
        if 'definitions' not in data:
            return RecursiveError('"definitions" property is required on body to begin training').to_json()

        training_samples = pd.DataFrame(data.get('training_samples'))
        definitions = data.get('definitions')
        output = p.train(training_samples, definitions)
        return output

    # @cherrypy.expose
    # @cherrypy.tools.json_in()
    # @cherrypy.tools.json_out()
    # def load_model(self):
    #     data = cherrypy.request.json
    #     response = LudwigProxy.loadModel(data.model_name)
    #     return response

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def predict(self):
        data = cherrypy.request.json
        response = LudwigProxy.predict(data['model_name'], json_normalize(data['data']))
        return response

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def index(self):
        return {'message': 'hello from ludwig service'}


def jsonify_error(status, message, traceback, version):
    response = cherrypy.response
    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'status': status, 'message': message, 'traceback': traceback.split('\n'), 'version': version})


# if __name__ == '__main__':
#     cherrypy.config.update({'server.socket_host': '0.0.0.0'})
#     cherrypy.config.update({'server.socket_port': 3333})
#     cherrypy.config.update({'error_page.default': jsonify_error})
#     cherrypy.quickstart(AiWebService())

global_conf = {
    'global': { 'engine.autoreload.on': False,
                'server.socket_host': '0.0.0.0',
                'server.socket_port': 3000,
                'error_page.default': jsonify_error,
                'log.screen': True,

                }}


if __name__ == '__main__':
    cherrypy.config.update(global_conf)

    # Disable request logging
    logger = cherrypy.log.access_log
    logger.removeHandler(logger.handlers[0])

    # Mount the application on the '/' base path
    app = cherrypy.tree.mount(AiWebService(), '/')

    # Initialize signal handler.
    if hasattr(cherrypy.engine, 'signal_handler'):
        cherrypy.engine.signal_handler.subscribe()

    # Initialize console control
    if hasattr(cherrypy.engine, "console_control_handler"):
        cherrypy.engine.console_control_handler.subscribe()

    # Start the engine.
    cherrypy.engine.start()

    # Running standalone so we need to block.
    cherrypy.engine.block()

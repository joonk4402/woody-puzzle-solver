import * as _ from 'lodash';
import { binaryNumber, CSV_DELIMITER, Entity } from './entity';

export class Piece extends Entity {
	private static readonly emptyValues: binaryNumber[][] = [
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
	];

	constructor(values: binaryNumber[][] = Piece.emptyValues) {
		super(values);
	}

	public clone() {
		return new Piece(_.cloneDeep(this._values));
	}

	/**
	 * Always output a piece with side 5x5
	 */
	public toCsv() {
		const maxSizedPiece = [
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
		];
		this.values.forEach((row: binaryNumber[], rowIndex: number) => {
			row.forEach((cellValue: binaryNumber, columnIndex: number) => {
				maxSizedPiece[rowIndex][columnIndex] = cellValue;
			});
		});
		return _.map(maxSizedPiece, (row) => row.join(CSV_DELIMITER)).join(CSV_DELIMITER);
	}
}

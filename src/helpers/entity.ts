import * as _ from 'lodash';
import { Board } from './board';
import { Piece } from './piece';

export const CSV_DELIMITER = ',';

export type binaryNumber = 0 | 1;
export type OneTwoThree = 0 | 1 | 2;
export type MoveInfo = {
	score?: number;
	order?: number;
	originalBoard: Board;
	board: Board;
	originalThreePieces?: Piece[];
	piece: Piece;
	pieceRowStart: number;
	pieceColStart: number;
	pieceIndex?: OneTwoThree;
};

export type ThreePieceMoveInfo = {
	firstMove: MoveInfo,
	secondMove: MoveInfo,
	thirdMove: MoveInfo,
};

export type SequentialMove = {
	move: MoveInfo;
	subsequentMoves?: SequentialMove[];
	pieceOrder?: number;
};

const EMPTY_CHARACTER = '0';
const FULL_CHARACTER = '1';

export abstract class Entity {
	protected readonly _values: binaryNumber[][];

	protected constructor(values: binaryNumber[][]) {
		this._values = values;
	}

	get width(): number {
		return this._values[0].length;
	}

	get height(): number {
		return this._values.length;
	}

	get values(): binaryNumber[][] {
		return this._values;
	}

	public toString(): string {
		let entityString = '';
		_.forEach(this._values, (row) => {
			let line: string = '';
			_.forEach(row, (cel: binaryNumber) => {
				if (cel) {
					line += FULL_CHARACTER;
				} else {
					line += EMPTY_CHARACTER;
				}
			});
			entityString += line + '\n';
		});
		return entityString;
	}

	public toCsv(): string {
		return _.map(this._values, (row) => row.join(CSV_DELIMITER)).join(CSV_DELIMITER);
	}

	public countEmpty(): number {
		return _.sumBy(_.flattenDeep<binaryNumber>(this._values), (cellValue) => Math.abs(cellValue - 1));
	}

	public countFilled(): number {
		return _.sum(_.flattenDeep<binaryNumber>(this._values));
	}

	public getRow(row: number): binaryNumber[] {
		return this._values[row];
	}

	public getColumn(col: number): binaryNumber[] {
		return _.map(this._values, (rowCells) => rowCells[col]);
	}

	public getCell(row: number, col: number): binaryNumber {
		return this._values[row][col];
	}

	public setCell(row: number, col: number, val: 1 | 0): binaryNumber {
		return this._values[row][col] = val;
	}
}

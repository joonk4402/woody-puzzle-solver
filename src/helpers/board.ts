import * as _ from 'lodash';
import { binaryNumber, Entity } from './entity';

export class Board extends Entity {
	private static readonly emptyBoardValues: binaryNumber[][] = [
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	];

	constructor(values: binaryNumber[][] = _.cloneDeep(Board.emptyBoardValues)) {
		super(values);
	}

	public clone() {
		return new Board(_.cloneDeep(this._values));
	}
}

import * as _ from 'lodash';
import { Piece } from './piece';
import { Board } from './board';
import { MoveInfo, OneTwoThree, SequentialMove, ThreePieceMoveInfo } from './entity';
import { LudwigServerProxy } from '../3-client/ludwig-server-proxy';

const emptyBoard: Board = new Board();
const emptyBoardCount: number = emptyBoard.width * emptyBoard.height;

export const pieces: Piece[] = [
	new Piece([
		[1, 1, 1],
		[1, 1, 1],
		[1, 1, 1],
	]),
	new Piece([
		[1, 1, 1, 1, 1],
	]),
	new Piece(
		[
			[1],
			[1],
			[1],
			[1],
			[1],
		]),
	new Piece(
		[
			[1],
			[1],
			[1],
			[1],
		]),
	new Piece(
		[
			[1],
			[1],
			[1],
		]),
	new Piece(
		[
			[1],
			[1],
		]),
	new Piece(
		[
			[1],
		]),
	new Piece(
		[
			[1, 1],
			[1, 1],
		]),
	new Piece(
		[
			[0, 0, 1],
			[0, 0, 1],
			[1, 1, 1],
		]),
	new Piece(
		[
			[1, 1, 1],
			[1, 0, 0],
			[1, 0, 0],
		]),
	new Piece(
		[
			[1, 1, 1],
			[0, 0, 1],
			[0, 0, 1],
		]),
	new Piece(
		[
			[1, 0, 0],
			[1, 0, 0],
			[1, 1, 1],
		]),
	new Piece(
		[
			[1, 1],
			[1, 0],
		]),
	new Piece(
		[
			[0, 1],
			[1, 1],
		]),
	new Piece(
		[
			[1, 0],
			[1, 1],
		]),
	new Piece(
		[
			[1, 1],
			[0, 1],
		]),
];

interface IScorePrediction {
	score_predictions: { [index: string]: number };
}

export function getRandomPiece(): Piece {
	return pieces[Math.floor(Math.random() * pieces.length)];
}

export async function getBestPieceScoreBySinglePiece(board: Board, playablePieces: Piece[], options: IPlayMatchOptions): Promise<MoveInfo | null> {
	try {
		let moves: MoveInfo[] = await getMovesWithScores(board, playablePieces, options.modelName);
		if (moves.length) {
			// some moves was possible
			const bestScores = _.orderBy(moves, 'score', 'desc');
			return bestScores[0];
		} else {
			// No more moves are possible
			return null;
		}
	} catch (err) {
		console.error(err);
	}
}

export interface IPieceOrderInfo {
	pieces: Piece[];
	order: (0 | 1 | 2)[];
}

//
// export async function getBestPieceScoreByThreePieces(board: Board, playablePieces: Piece[], options: IPlayMatchOptions): Promise<ThreePieceMoveInfo | null> {
// 	// Get all possible piece positions
// 	let allPossiblePieceOrders: IPieceOrderInfo[] = [
// 		{ pieces: [playablePieces[0], playablePieces[1], playablePieces[2]], order: [0, 1, 2] },
// 		{ pieces: [playablePieces[0], playablePieces[2], playablePieces[1]], order: [0, 1, 2] },
// 		{ pieces: [playablePieces[1], playablePieces[0], playablePieces[2]], order: [0, 1, 2] },
// 		{ pieces: [playablePieces[1], playablePieces[2], playablePieces[0]], order: [0, 1, 2] },
// 		{ pieces: [playablePieces[2], playablePieces[0], playablePieces[1]], order: [0, 1, 2] },
// 		{ pieces: [playablePieces[2], playablePieces[1], playablePieces[0]], order: [0, 1, 2] },
// 	];
// 	const possibleMoves: ThreePieceMoveInfo[] = [];
// 	allPossiblePieceOrders.forEach((piecesInfo) => {
// 		const moves: MoveInfo[] = getPossibleMovesRecursively(board, piecesInfo);
// 		possibleMoves.push(..._.map(moves, (move): ThreePieceMoveInfo => {
// 			// Convert single piece move to 3 piece move
// 			const threePieceMove = move as Partial<ThreePieceMoveInfo>;
// 			threePieceMove.firstPieceIndex = piecesInfo.order[0];
// 			threePieceMove.secondPieceIndex = piecesInfo.order[1];
// 			threePieceMove.firstPieceColStart =
// 			return threePieceMove as ThreePieceMoveInfo;
// 		}));
// 	});
// 	// find best move by calculating a score for all possible moves using boardEvaluator neural net which was trained in step 3
// 	await calculateScoreForMoves(possibleMoves, options.modelName);
// 	const bestMove: MoveInfo = _.orderBy(possibleMoves, (move: MoveInfo) => move.score)[0];
// 	bestMove.originalThreePieces = pieces;
// 	return bestMove;
// }
//
// function getPossibleMovesRecursively(board: Board, pieces: Piece[], sequentialMoveInfo: SequentialMove, level: number): void { // all possible 3 moves
// 	let piece = pieceOrderInfo.pieces.shift();
// 	let moves = getPossibleMoves(board, piece);
// 	sequentialMoveInfo.subsequentMoves = moves.map((move: MoveInfo) => {
// 		return {
// 			move
// 		}
// 	});
// 	if (level >= 2) {
// 		return;
// 	} else {
//
// 	}
//
//
// 	// if (!possibleThreePieceMoves[0].firstMove) {
// 	//
// 	// }
// 	// if (pieces.length) {
// 	// 	const possibleMovesDeep: MoveInfo[] = [];
// 	// 	possibleMoves.forEach((possibleMove) => {
// 	// 		possibleMovesDeep.push(...getPossibleMovesRecursively(possibleMove.board, pieces));
// 	// 	});
// 	// 	return possibleMovesDeep;
// 	// } else {
// 	// 	return;
// 	// }
// }

export function findPieceIndex(pieceArray: (Piece | undefined)[], piece: Piece): number {
	// printPieces(pieceArray);
	for (let i = 0; i < pieceArray.length; i++) {
		if (pieceArray[i] && pieceArray[i].toString() === piece.toString()) {
			return i;
		}
	}
	throw 'findPieceIndex did not find the piece in the provided array';
}

export function findPieceIndexes(originalPieceArray: Piece[], playOrderPieceArray: (Piece | undefined)[]): OneTwoThree[] {
	const playOrder: OneTwoThree[] = [];
	playOrderPieceArray.forEach((playOrderPiece: Piece | undefined, playOrderPieceIndex: OneTwoThree) => {
		originalPieceArray.forEach((originalPiece: Piece, originalPieceIndex: OneTwoThree) => {
			if (playOrderPiece && originalPiece.toString() === playOrderPiece.toString() && _.isUndefined(playOrder[playOrderPieceIndex])) {
				playOrder[playOrderPieceIndex] = originalPieceIndex;
			}
		});
	});
	return playOrder;
}

async function getMovesWithScores(board: Board, playablePieces: Piece[], modelName?: string): Promise<MoveInfo[]> {
	const moves: MoveInfo[] = _.flatten(_.map(playablePieces, (piece: Piece, index: OneTwoThree): MoveInfo[] => {
		// Get all possible boards for this random piece
		return getPossibleMoves(board, piece, index);
	}));
	await calculateScoreForMoves(moves, modelName);
	return moves;
}

export function getRandomMove(board: Board, playablePieces: Piece[], playableIndexes: OneTwoThree[]): MoveInfo {
	// console.log('getRandomMove for --------------');
	// printPieces(playablePieces);
	// console.log('playable indexes\n', playableIndexes);
	const moves: MoveInfo[] = getAllPossibleMoves(board, playablePieces, playableIndexes);
	return _.sample(moves);
}

function getAllPossibleMoves(board: Board, playablePieces: Piece[], playableIndexes: OneTwoThree[]): MoveInfo[] {
	return _.flatten(_.map(playablePieces, (piece: Piece, index: OneTwoThree): MoveInfo[] => {
		// Get all possible boards for this random piece
		return getPossibleMoves(board, piece, playableIndexes[index]);
	}));
}

export async function calculateScoreForMoves(moves: MoveInfo[], modelName?: string): Promise<void> {
	let scores: number[];
	if (modelName) {
		scores = await getPredictionsAi(_.map(moves, 'board'), modelName);
	} else {
		scores = await getPredictionsDummy(_.map(moves, 'board'));
	}
	_.forEach(moves, (move: MoveInfo, index: number) => {
		move.score = scores[index];
	});
}

function getPredictionsDummy(boards: Board[]): Promise<number[]> {
	return Promise.resolve(_.map(boards, (board) => {
		return board.countFilled() / emptyBoardCount + Math.random() * 0.1;
	}));
}

export interface IPlayMatchOptions {
	generation?: number;
	numberOfMatches?: number;
	batchSize?: number;
	modelName?: string;
}

async function getPredictionsAi(boards: Board[], modelName: string): Promise<number[]> {
	try {
		if (!boards || boards.length === 0) {
			return Promise.resolve([]);
		}

		const data = _.map(boards, (board: Board) => {
			const data: any = { score: undefined };
			_.forEach(_.flattenDeep(board.values), (value, index) => {
				data['column_' + index] = value;
			});
			return data;
		});
		let predictions: number[];
		if (modelName) {
			predictions = _.values((await LudwigServerProxy.getPrediction(modelName, data) as IScorePrediction).score_predictions);
		} else {
			predictions = await getPredictionsDummy(data);
		}
		return _.values(predictions);
	} catch (err) {
		console.error(err);
	}
}

// function getLudwigCsvHeader(): string {
// 	return _.times(100, (columnIndex: number) => {
// 		return 'column_' + columnIndex;
// 	}) + ',score';
// }

/**
 * Returns all the boards after each possible move that the system can make with the provided piece placed in all valid positions
 * @param board
 * @param {Piece} piece
 * @return {Board[]}
 */
export function getPossibleMoves(board: Board, piece: Piece, index: OneTwoThree): MoveInfo[] {
	const moves: MoveInfo[] = [];
	for (let row = 0; row < emptyBoard.height; row++) {
		for (let col = 0; col < emptyBoard.width; col++) {
			moves.push(placePieceIfValid(board, piece, row, col));
		}
	}
	return _.map(_.compact(moves), (move) => {
		move.pieceIndex = index;
		return move;
	});
}

/**
 * Return a moveInfo object containing the board after the piece has been placed or if the piece cannot be placed it returns null
 * @param originalBoard
 * @param {Piece} piece
 * @param {number} pieceRowStart
 * @param {number} pieceColStart
 * @return {Board | null}
 */
export function placePieceIfValid(originalBoard: Board, piece: Piece, pieceRowStart: number, pieceColStart: number): MoveInfo | null {
	if (pieceRowStart + piece.height > originalBoard.height ||
		pieceColStart + piece.width > originalBoard.width) {
		// Piece is partly outside of the board
		return null;
	}
	// Fully inside
	let board = originalBoard.clone();
	for (let boardRow = pieceRowStart, pieceRow = 0; boardRow < board.height && pieceRow < piece.height; boardRow++, pieceRow++) {
		for (let boardCol = pieceColStart, pieceCol = 0; boardCol < board.width && pieceCol < piece.width; boardCol++, pieceCol++) {
			if (board.getCell(boardRow, boardCol) === 1 && piece.getCell(pieceRow, pieceCol) === 1) {
				// part of piece collides with blocks already on the board
				return null;
			} else if (board.getCell(boardRow, boardCol) === 0 && piece.getCell(pieceRow, pieceCol) === 1) {
				board.setCell(boardRow, boardCol, 1);
			}
		}
	}
	removeFullRows(board);
	return {
		originalBoard,
		board,
		piece: piece,
		pieceRowStart,
		pieceColStart,
	};
}

//
// export function isValidPlacement(board: Board, piece: Piece, pieceRowStart: number, pieceColStart: number): boolean {
// 	for (let boardRow = pieceRowStart, pieceRow = 0; boardRow < board.height && pieceRow < piece.height; boardRow++, pieceRow++) {
// 		for (let boardCol = pieceColStart, pieceCol = 0; boardCol < board.width && pieceCol < piece.width; boardCol++, pieceCol++) {
// 			if (board.getCell(boardRow, boardCol) === 1 && piece.getCell(pieceRow, pieceCol) === 1) {
// 				// part of piece collides with blocks already on the board
// 				return false;
// 			}
// 		}
// 	}
// 	return true;
// }

export function removeFullRows(board: Board) {
	let rowIndices: number[] = [];
	let colIndices: number[] = [];
	// Identify full rows
	for (let boardRow = 0; boardRow < board.height; boardRow++) {
		if (_.every(board.getRow(boardRow), (cellValue) => cellValue === 1)) {
			rowIndices.push(boardRow);
		}
	}
	// Identify full columns
	for (let boardCol = 0; boardCol < board.width; boardCol++) {
		if (_.every(board.getColumn(boardCol), (cellValue) => cellValue === 1)) {
			colIndices.push(boardCol);
		}
	}
	// Delete full rows
	_.forEach(rowIndices, (rowIndex) => {
		_.forEach(board.getRow(rowIndex), (value: number, colIndex: number) => {
			board.setCell(rowIndex, colIndex, 0);
		});
	});
	// Delete full columns
	_.forEach(colIndices, (colIndex) => {
		_.forEach(board.getColumn(colIndex), (value: number, rowIndex: number) => {
			board.setCell(rowIndex, colIndex, 0);
		});
	});
}

export function printBoard(board: Board) {
	console.log(board.toString() + '\n');
}

export function printPieces(pieces: (Piece | undefined)[]) {
	console.log(pieces.map((piece: Piece | undefined) => {
		return piece ? piece.toString() : '[undefined piece]\n';
	}).join('-----------------\n'));
	console.log('##################');
}

export function calculateBoardScoreDummy(move: MoveInfo, maxMatchLength: number) {
	return move.order / maxMatchLength;
}

import { Board } from '../helpers/board';
import { binaryNumber } from '../helpers/entity';
import { pieces } from '../helpers/solver';
import _ = require('lodash');

const bent = require('bent');
const post = bent('http://localhost:3000/', 'POST', 'json', 200);

export class LudwigServerProxy {
	public static async getPrediction(modelName: string, data: any[]): Promise<any> {
		let response;
		try {
			response = await post('predict', {
				model_name: modelName,
				data,
			});
		} catch (err) {
			if (err.responseBody) {
				const responseBody = await err.responseBody;
				let jsonResponse;
				try {
					jsonResponse = JSON.parse(responseBody.toString());
				} catch (jsonErr) {
					throw new Error('Failed to get prediction from ludwig: ' + responseBody.toString());
				}
				if (jsonResponse.traceback) {
					throw new Error('Failed to get prediction from ludwig: ' + jsonResponse.traceback.join('\n'));
				}
				throw new Error('Failed to get prediction from ludwig: ' + JSON.stringify(jsonResponse, null, 2));
			} else {
				throw new Error('Failed to get prediction from ludwig: ' + JSON.stringify(err, null, 2));
			}
		}
		if (response.message === 'ok') {
			return response.results;
		} else {
			throw new Error('Failed to get prediction from ludwig: ' + JSON.stringify(response, null, 2));
		}
	}
}

(async () => {
	try {
		const board = new Board();
		const row = {};
		_.flatten(board.values).forEach((boardValue: binaryNumber, index: number) => {
			row['board_column_' + index] = boardValue;
		});
		const firstPieceValues = pieces[0].toCsv().split(',');
		const secondPieceValues = pieces[3].toCsv().split(',');
		const thirdPieceValues = pieces[5].toCsv().split(',');
		firstPieceValues.forEach((pieceValue: string, index: number) => {
			row['first_piece_column_' + index] = pieceValue;
		});
		secondPieceValues.forEach((pieceValue: string, index: number) => {
			row['second_piece_column_' + index] = pieceValue;
		});
		thirdPieceValues.forEach((pieceValue: string, index: number) => {
			row['third_piece_column_' + index] = pieceValue;
		});
		const outputColumns = ['first_piece_is_played_first', 'second_piece_is_played_first', 'third_piece_is_played_first', 'first_piece_is_played_second', 'second_piece_is_played_second', 'third_piece_is_played_second', 'first_piece_is_played_third', 'second_piece_is_played_third', 'third_piece_is_played_third', 'first_piece_x', 'first_piece_y', 'second_piece_x', 'second_piece_y', 'third_piece_x', 'third_piece_y', 'score'];
		outputColumns.forEach((columnName) => {
			row[columnName] = '';
		});
		console.log(await LudwigServerProxy.getPrediction('three-piece-game-1000-gen-0', [row]));
	} catch (err) {
		console.log(err);
	}
})();

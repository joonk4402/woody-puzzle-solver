import { Worker } from 'worker_threads';
import * as promiseUtils from 'blend-promise-utils';
import * as _ from 'lodash';
import * as path from 'path';
import * as fs from 'fs-extra';

const uuidv1 = require('uuid/v1');

import { binaryNumber, CSV_DELIMITER, ThreePieceMoveInfo } from '../helpers/entity';
import { options } from './options';
import { Board } from '../helpers/board';
import { Piece } from '../helpers/piece';
import { start } from 'repl';

let matchesFinished = 0;
let lastPercentageReported = 0;

const startTime = Date.now();

function runService() {
	return new Promise((resolve, reject) => {
		const worker = new Worker('./src/1-prepare-model/4-generate-training-data-piece-placement.js', {});
		worker.on('message', async (response: { message: ThreePieceMoveInfo[][] }) => {

			// TODO change fitness function to use score instead of match length
			const trainingRuns = _.flatten(response.message);

			// console.log('finished training ' + trainingRuns.length + 'runs');

			// output to csv
			const dirName = `three-piece-game-${options.batchSize}-gen-0`;
			const csvPath = path.join(__dirname, `../../data/${dirName}/${dirName}-${uuidv1()}.csv`);
			await fs.ensureFile(csvPath);
			const trainingDataFileDescriptor: number = await fs.open(csvPath, 'a');

			await fs.write(trainingDataFileDescriptor, generateOldCsvHeaders() + '\n', 0);
			await promiseUtils.mapLimit(trainingRuns, 10, async (trainingItem: ThreePieceMoveInfo) => {
				const csvString = threePieceMoveInfoToCsv(trainingItem);
				await fs.write(trainingDataFileDescriptor, csvString + '\n', 0);
			});
			await fs.close(trainingDataFileDescriptor);

			// console.log('finished writing file: ' + csvPath);

			matchesFinished += options.batchSize;
			const currentTime = Date.now();
			const percentage = Math.round(matchesFinished / options.numberOfMatches * 100);
			if (percentage !== lastPercentageReported) {
				const expectedRemainingTime = Math.round((currentTime - startTime) / percentage * (100 - percentage) / 1000 / 60);
				const averageSpeed = Math.round(matchesFinished / (currentTime - startTime) * 1000 * 60);
				console.log('played matches: ' + matchesFinished + '\t' + percentage + '%\t' + expectedRemainingTime + ' minutes remaining\t' + averageSpeed + ' matches/min');
				lastPercentageReported = percentage;
			}

			resolve();
		});
		worker.on('error', reject);
		worker.on('exit', (code: number | string) => {
			if (code !== 0) {
				reject(new Error(`Worker stopped with exit code ${code}`));
			}
		});
	});
}

(async () => {
	console.log('initializing treads');
	await promiseUtils.map(_.times(options.threads), runService);
})();

function threePieceMoveInfoToCsv(moveInfo: ThreePieceMoveInfo): string {
	return convertBoardToCsv(moveInfo.firstMove.originalBoard) + CSV_DELIMITER +
		convertPieceToCsv(moveInfo.firstMove.piece) + CSV_DELIMITER +
		convertPieceToCsv(moveInfo.secondMove.piece) + CSV_DELIMITER +
		convertPieceToCsv(moveInfo.thirdMove.piece) + CSV_DELIMITER +
		moveInfo.firstMove.pieceIndex + CSV_DELIMITER +
		moveInfo.secondMove.pieceIndex + CSV_DELIMITER +
		moveInfo.firstMove.pieceColStart + CSV_DELIMITER +
		moveInfo.firstMove.pieceRowStart + CSV_DELIMITER +
		moveInfo.secondMove.pieceColStart + CSV_DELIMITER +
		moveInfo.secondMove.pieceRowStart + CSV_DELIMITER +
		moveInfo.thirdMove.pieceColStart + CSV_DELIMITER +
		moveInfo.thirdMove.pieceRowStart + CSV_DELIMITER +
		moveInfo.firstMove.order;
}

function convertBoardToCsv(board: Board): string {
	// Stringified and parsed response from worker thread => access _values directly
	return _.map((board as unknown as { _values: binaryNumber[][] })._values, (row) => row.join(CSV_DELIMITER)).join(CSV_DELIMITER);
}

function convertPieceToCsv(piece: Piece): string {
	const maxSizedPiece = [
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0],
	];

	// Stringified and parsed response from worker thread => access _values directly
	(piece as unknown as { _values: binaryNumber[][] })._values.forEach((row: binaryNumber[], rowIndex: number) => {
		row.forEach((cellValue: binaryNumber, columnIndex: number) => {
			maxSizedPiece[rowIndex][columnIndex] = cellValue;
		});
	});
	return _.map(maxSizedPiece, (row) => row.join(CSV_DELIMITER)).join(CSV_DELIMITER);
}

export function generateOldCsvHeaders(): string {
	// Construct columns
	// Input columns
	const boardColumns = _.times(100, (columnIndex: number) => 'board_column_' + columnIndex);
	const firstPieceColumns = _.times(25, (columnIndex: number) => 'first_piece_column_' + columnIndex);
	const secondPieceColumns = _.times(25, (columnIndex: number) => 'second_piece_column_' + columnIndex);
	const thirdPieceColumns = _.times(25, (columnIndex: number) => 'third_piece_column_' + columnIndex);

	// output columns
	return [
		...boardColumns,
		...firstPieceColumns,
		...secondPieceColumns,
		...thirdPieceColumns,
		'first_piece_index', // TODO replace with 9 simple binary type columns
		'second_piece_index',
		'first_piece_x',
		'first_piece_y',
		'second_piece_x',
		'second_piece_y',
		'third_piece_x',
		'third_piece_y',
		'order',
	].join(CSV_DELIMITER);
}

/**
 * Converts the order column of generated matches to a score between 0 and 1
 * order indicates how many more moves the game lasted after the current board state
 * score equals the order of the current board divided by the max number of moves for a certain batch
 */

import * as fs from 'fs-extra';
import * as path from 'path';
import * as _ from 'lodash';
import * as promiseUtils from 'blend-promise-utils';
import { CSV_DELIMITER } from '../helpers/entity';
import { features } from './3-generate-train-command';

async function getMaxOrder(filePath: string): Promise<number> {
	let maxOrder = 0;
	(await fs.readFile(filePath)).toString().split('\n').forEach((boardRow: string, index: number) => {
		if (index !== 0) {
			const orderString = boardRow.split(CSV_DELIMITER).pop();
			const order = parseInt(orderString);
			if (order > maxOrder) {
				maxOrder = order;
			}
		}
	});
	return maxOrder;
}

async function updateRows(filePath: string, maxOrder: number, uniqueValues: Set<string>): Promise<string> {
	const oldRows = (await fs.readFile(filePath)).toString().split('\n').filter(r => r.length > 0);
	const oldHeaders = oldRows.shift().split(CSV_DELIMITER);
	const firstPieceIndex = oldHeaders.indexOf('first_piece_index');
	const secondPieceIndex = oldHeaders.indexOf('second_piece_index');
	const rows: string[] = oldRows.map((boardRow: string, index: number): string => {
		const cellStrings: string[] = boardRow.split(CSV_DELIMITER);
		cellStrings.forEach((v) => uniqueValues.add(v));
		const firstPieceIsPlayedFirst = cellStrings[firstPieceIndex] === '0';
		const secondPieceIsPlayedFirst = cellStrings[firstPieceIndex] === '1';
		const thirdPieceIsPlayedFirst = !firstPieceIsPlayedFirst && !secondPieceIsPlayedFirst;
		const firstPieceIsPlayedSecond = cellStrings[secondPieceIndex] === '0';
		const secondPieceIsPlayedSecond = cellStrings[secondPieceIndex] === '1';
		const thirdPieceIsPlayedSecond = !firstPieceIsPlayedSecond && !secondPieceIsPlayedSecond;
		const firstPieceIsPlayedThird = cellStrings[firstPieceIndex] !== '0' && cellStrings[secondPieceIndex] !== '0';
		const secondPieceIsPlayedThird = cellStrings[firstPieceIndex] !== '1' && cellStrings[secondPieceIndex] !== '1';
		const thirdPieceIsPlayedThird = !firstPieceIsPlayedThird && !secondPieceIsPlayedThird;
		const order = parseInt(_.last(cellStrings));

		cellStrings.splice(firstPieceIndex, 2, ...[
			firstPieceIsPlayedFirst ? '1' : '0',
			secondPieceIsPlayedFirst ? '1' : '0',
			thirdPieceIsPlayedFirst ? '1' : '0',
			firstPieceIsPlayedSecond ? '1' : '0',
			secondPieceIsPlayedSecond ? '1' : '0',
			thirdPieceIsPlayedSecond ? '1' : '0',
			firstPieceIsPlayedThird ? '1' : '0',
			secondPieceIsPlayedThird ? '1' : '0',
			thirdPieceIsPlayedThird ? '1' : '0',
		]);
		cellStrings[cellStrings.length - 1] = String(order / maxOrder);
		return cellStrings.join(CSV_DELIMITER);
	});
	return rows.join('\n');
}

export function generateNewCsvHeaders(): string {
	// Construct columns
	// Input columns
	const boardColumns = _.times(100, (columnIndex: number) => 'board_column_' + columnIndex);
	const firstPieceColumns = _.times(25, (columnIndex: number) => 'first_piece_column_' + columnIndex);
	const secondPieceColumns = _.times(25, (columnIndex: number) => 'second_piece_column_' + columnIndex);
	const thirdPieceColumns = _.times(25, (columnIndex: number) => 'third_piece_column_' + columnIndex);

	// output columns
	return [
		...boardColumns,
		...firstPieceColumns,
		...secondPieceColumns,
		...thirdPieceColumns,
		'first_piece_is_played_first',
		'second_piece_is_played_first',
		'third_piece_is_played_first',
		'first_piece_is_played_second',
		'second_piece_is_played_second',
		'third_piece_is_played_second',
		'first_piece_is_played_third',
		'second_piece_is_played_third',
		'third_piece_is_played_third',
		'first_piece_x',
		'first_piece_y',
		'second_piece_x',
		'second_piece_y',
		'third_piece_x',
		'third_piece_y',
		'order',
	].join(CSV_DELIMITER);
}

function increaseOperations(operationsCompleted: number, totalOperations: number): number {
	let rounding = 0;
	if (operationsCompleted < 0.10 * totalOperations) {
		rounding = 1;
	}
	const lastPercentage = _.round(operationsCompleted * 100 / totalOperations, rounding);
	operationsCompleted += 1;
	const currentPercentage = _.round(operationsCompleted * 100 / totalOperations, rounding);
	if (currentPercentage !== lastPercentage) {
		console.log(currentPercentage + ' %');
	}
	return operationsCompleted;
}

async function processMatches() {
	try {
		const dataFolderPath = path.join(__dirname, '../../data/three-piece-game-1000-gen-0');
		const fileNames = await fs.readdir(dataFolderPath);

		console.log(`processing ${fileNames.length} match files`);

		let operationsCompleted = 0;

		// Find max number of moves
		let maxOrder = 0;
		await promiseUtils.mapLimit(fileNames, 10, async (fileName: string) => {
			const maxFileOrder: number = await getMaxOrder(path.join(dataFolderPath, fileName));
			if (maxFileOrder > maxOrder) {
				maxOrder = maxFileOrder;
			}
			operationsCompleted = increaseOperations(operationsCompleted, fileNames.length * 2);
		});

		console.log('finished finding highest order: ', maxOrder);

		// Convert number of moves to score + write to single file
		const combinedFilePath = path.join(__dirname, '../../data/data-three-piece-game-gen-0.csv');
		await fs.remove(combinedFilePath);
		const combinedFileDescriptor: number = await fs.open(combinedFilePath, 'a');
		const csvHeaders = generateNewCsvHeaders().replace(/,order$/, ',score\n');
		await fs.write(combinedFileDescriptor, csvHeaders, 0);
		const uniqueValues = new Set<string>();
		await promiseUtils.mapLimit(fileNames, 10, async (fileName: string) => {
			const csvString = await updateRows(path.join(dataFolderPath, fileName), maxOrder, uniqueValues) + '\n';
			await fs.write(combinedFileDescriptor, csvString, 0);
			operationsCompleted = increaseOperations(operationsCompleted, fileNames.length * 2);
		});

		await fs.close(combinedFileDescriptor);
		console.log('done');
	} catch (err) {
		console.error(err);
	}
}

processMatches();

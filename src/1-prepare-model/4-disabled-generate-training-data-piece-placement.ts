// import { getRandomPiece, IPlayMatchOptions } from '../helpers/solver';
// import { MoveInfo, ThreePieceMoveInfo } from '../helpers/entity';
// import { Board } from '../helpers/board';
// import { Piece } from '../helpers/piece';
// import * as _ from 'lodash';
// import * as promiseUtils from 'blend-promise-utils';
// import * as fs from 'fs-extra';
// import * as path from 'path';
//
// const options = {
// 	generation: 1, // '1-trained-on-1000';
// 	numberOfMatches: 1,
// 	batchSize: 1,
// 	modelName: `data-1000000-gen-0`,
// };
//
// async function generatePlacementTrainingData() {
// 	const trainingData: ThreePieceMoveInfo[] = [];
//
// 	await promiseUtils.mapLimit(_.times(options.numberOfMatches), options.batchSize, async () => {
// 		trainingData.push(...(await playPerfectMatch((options))));
// 	});
//
// 	// output to csv
// 	const trainingDataFileDescriptor: number = await fs.open(path.join(__dirname, `../data/perfect-game-${options.numberOfMatches}-gen-0.csv`), 'a');
// 	// Construct columns
// 	// Input columns
// 	const boardColumns = _.times(100, (columnIndex: number) => 'board_column_' + columnIndex).join(CSV_DELIMITER);
// 	const firstPieceColumns = _.times(25, (columnIndex: number) => 'first_piece_column_' + columnIndex).join(CSV_DELIMITER);
// 	const secondPieceColumns = _.times(25, (columnIndex: number) => 'second_piece_column_' + columnIndex).join(CSV_DELIMITER);
// 	const thirdPieceColumns = _.times(25, (columnIndex: number) => 'third_piece_column_' + columnIndex).join(CSV_DELIMITER);
//
// 	// Calculate score for training items
// 	const maxOrder: number = _.maxBy(trainingData, (trainingItem: ThreePieceMoveInfo) => {
// 		return trainingItem.thirdMove.order;
// 	}).thirdMove.order;
// 	trainingData.forEach((trainingItem: ThreePieceMoveInfo) => {
// 		trainingItem.thirdMove.score = trainingItem.thirdMove.order / maxOrder;
// 	});
//
// // output columns
// 	const csvHeaders: string = [
// 		...boardColumns,
// 		...firstPieceColumns,
// 		...secondPieceColumns,
// 		...thirdPieceColumns,
// 		'first_piece_index',
// 		'second_piece_index',
// 		'first_piece_x',
// 		'first_piece_y',
// 		'second_piece_x',
// 		'second_piece_y',
// 		'third_piece_x',
// 		'third_piece_y',
// 		'score',
// 	].join(CSV_DELIMITER);
// 	await fs.write(trainingDataFileDescriptor, csvHeaders, 0);
// 	await promiseUtils.mapLimit(trainingData, 10, async (trainingItem: ThreePieceMoveInfo) => {
// 		const csvString = '';
// 		trainingItem.
// 		await fs.write(combinedFileDescriptor, csvString, 0);
// 		operationsCompleted = increaseOperations(operationsCompleted, fileNames.length * 2);
// 	});
// 	_.forEach(trainingData, (move: MoveInfo) => {
//
// 	});
// }
//
// async function playPerfectMatch(options: IPlayMatchOptions): Promise<ThreePieceMoveInfo[]> {
// 	let endGame = false;
// 	let consecutiveMoves: ThreePieceMoveInfo[] = [];
// 	let board = new Board();
// 	let movesPlayedSoFar = 0;
// 	while (!endGame) {
// 		let playablePieces: Piece[] = _.times(3, getRandomPiece);
// 		console.log('moved played so far: ' + movesPlayedSoFar);
// 		const moveInfo = await getBestPieceScoreByThreePieces(board, playablePieces, { modelName: 'data-1000000-gen-0' });
// 		consecutiveMoves.push(moveInfo);
// 	}
// 	return consecutiveMoves;
// }
//
//
// generatePlacementTrainingData();

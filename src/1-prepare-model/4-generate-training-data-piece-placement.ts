import { CSV_DELIMITER, ThreePieceMoveInfo } from '../helpers/entity';
import * as _ from 'lodash';
import * as promiseUtils from 'blend-promise-utils';
import { playRandomMatch } from '../helpers/play-random-matches';
import { options } from './options';

const { parentPort } = require('worker_threads');

function generatePlacementTrainingDataBatch(batchSize: number): ThreePieceMoveInfo[][] {
	const trainingData: ThreePieceMoveInfo[][] = [];
	for (let i = 0; i < batchSize; i++) {
		trainingData.push(playRandomMatch());
	}

	return trainingData;
}

function generatePlacementTrainingData() {
	for (let i = 0; i < options.numberOfMatches / options.threads / options.batchSize; i++) {
		const trainingData = generatePlacementTrainingDataBatch(options.batchSize);
		parentPort.postMessage({ message: trainingData });
	}
	// const trainingDatas: ThreePieceMoveInfo[][][] = await promiseUtils.mapLimit(
	// 	_.times( options.batchSize),
	// 	10,
	// 	async () => {
	// 		// console.log('running batch: ' + options.batchSize);
	// 		const batch = await generatePlacementTrainingDataBatch(options.batchSize);
	// 		return batch;
	// 	});
	//
	// const trainingData: ThreePieceMoveInfo[][] = _.flatten(trainingDatas);
	//
	// parentPort.postMessage({ message: trainingData });
}

// async function generatePlacementTrainingData() {
// 	console.log('Playing ' + options.numberOfMatches + ' matches');
// 	await promiseUtils.mapLimit(
// 		_.times(options.numberOfMatches / options.batchSize),
// 		20,
// 		async (batchIndex: number) => {
// 			await generatePlacementTrainingDataBatch(options.batchSize);
// 			console.log('matches played: ' + (batchIndex + 1) * options.batchSize + '   ' + _.round(((batchIndex + 1) * options.batchSize) / options.numberOfMatches, 1) + '%');
// 		});
// 	console.log('done');
// }

generatePlacementTrainingData();


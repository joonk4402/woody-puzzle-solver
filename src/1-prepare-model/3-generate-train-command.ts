/**
 * Generates the train command to train a neural net to provide a score for a certain board state
 */

import * as fs from 'fs-extra';
import * as _ from 'lodash';
import * as path from 'path';

// ludwig train --data_csv data-1000.csv --model_definition "{input_features: [{name: doc_text, type: text}], output_features: [{name: class, type: category}]}"

const board_columns = _.times(100, (columnIndex: number) => {
	return { name: 'board_column_' + columnIndex, type: 'binary' };
});
const first_piece_columns = _.times(25, (columnIndex: number) => {
	return { name: 'first_piece_column_' + columnIndex, type: 'binary' };
});
const second_piece_columns = _.times(25, (columnIndex: number) => {
	return { name: 'second_piece_column_' + columnIndex, type: 'binary' };
});
const third_piece_columns = _.times(25, (columnIndex: number) => {
	return { name: 'third_piece_column_' + columnIndex, type: 'binary' };
});

export const features = {
	input_features: [
		...board_columns,
		...first_piece_columns,
		...second_piece_columns,
		...third_piece_columns,
	],
	output_features: [
		{ name: 'first_piece_is_played_first', type: 'binary' },
		{ name: 'second_piece_is_played_first', type: 'binary' },
		{ name: 'third_piece_is_played_first', type: 'binary' },
		{ name: 'first_piece_is_played_second', type: 'binary' },
		{ name: 'second_piece_is_played_second', type: 'binary' },
		{ name: 'third_piece_is_played_second', type: 'binary' },
		{ name: 'first_piece_is_played_third', type: 'binary' },
		{ name: 'second_piece_is_played_third', type: 'binary' },
		{ name: 'third_piece_is_played_third', type: 'binary' },
		{ name: 'first_piece_x', type: 'numerical' },
		{ name: 'first_piece_y', type: 'numerical' },
		{ name: 'second_piece_x', type: 'numerical' },
		{ name: 'second_piece_y', type: 'numerical' },
		{ name: 'third_piece_x', type: 'numerical' },
		{ name: 'third_piece_y', type: 'numerical' },
		{ name: 'score', type: 'numerical' }
	]
};
let featuresString = JSON.stringify(features)
	.replace(/"/g, '')
	.replace(/:/g, ': ');
let command = `ludwig train --data_csv data-three-piece-game-gen-0.csv --model_definition "${featuresString}"`;

fs.writeFileSync(path.join(__dirname, '../../data/train-ludwig.bat'), command);
